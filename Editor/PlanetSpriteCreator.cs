using UnityEngine;
using UnityEditor;

public class PlanetSpriteCreator : EditorWindow
{
	int amount = 24;
	int res = 128;
	string filePath = "";
	string fileName = "moon";

	Light light;
	Camera cam;

	[MenuItem("Tools/Planet Sprite Creator")]
	static void Init()
	{
		var window = GetWindow(typeof(PlanetSpriteCreator));
		window.Show();
	}

	void OnGUI()
	{
		EditorGUILayout.LabelField("Pictures", EditorStyles.boldLabel);
		amount = EditorGUILayout.IntSlider("Amount", amount, 1, 256);
		res = EditorGUILayout.IntField("Resolution", res);

		EditorGUILayout.Space();

		EditorGUILayout.LabelField("Save File", EditorStyles.boldLabel);
		EditorGUILayout.TextField("Path", filePath);
		if (filePath == "")
			filePath = Application.dataPath + "/Moon Sprites";
		if (GUILayout.Button("Browse"))
			filePath = EditorUtility.SaveFolderPanel("Select Folder", filePath, Application.dataPath);
		EditorGUILayout.TextField("Name", fileName);

		EditorGUILayout.Space();

		EditorGUILayout.LabelField("Objects", EditorStyles.boldLabel);
		cam = EditorGUILayout.ObjectField("Camera", cam, typeof(Camera), true) as Camera;
		if (cam == null)
			cam = Camera.main;

		EditorGUILayout.Space();

		if (GUILayout.Button("Create Sprites"))
		{
			cam.targetTexture = new RenderTexture(res, res, 24, RenderTextureFormat.ARGBHalf);
			RenderTexture.active = cam.targetTexture;
			var tex = new Texture2D(cam.targetTexture.width, cam.targetTexture.height, TextureFormat.RGBAHalf, false, true);

			var adder = 360f / amount;
			for (int i = 0; i < amount; i++)
			{
				var rot = new Vector3(0, i * adder, 0);
				cam.transform.eulerAngles = rot;

				cam.Render();
				tex.ReadPixels(new Rect(0, 0, tex.width, tex.height), 0, 0);
				System.IO.File.WriteAllBytes(string.Format("{0}/" + fileName + "_{1}.exr", filePath, i), tex.EncodeToEXR(Texture2D.EXRFlags.CompressZIP));
			}
			cam.targetTexture = null;
			RenderTexture.active = null;
			cam.transform.eulerAngles = new Vector3(0, 0, 0);

			Debug.Log(amount + " images saved to " + filePath);
		}
	}
}
